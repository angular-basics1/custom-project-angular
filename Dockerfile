FROM node:18.10.0

WORKDIR /home/node/app
COPY package*.json ./

RUN npm install -g @angular/cli @angular-devkit/build-angular && npm install

# Exposition du port de node js
EXPOSE 4200

CMD ["npm", "start"]
